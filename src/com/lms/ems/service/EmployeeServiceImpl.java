package com.lms.ems.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import com.lms.ems.domain.Employee;
import com.lms.ems.exception.DuplicationEmailException;

public class EmployeeServiceImpl implements EmployeeService {
	private static EmployeeServiceImpl employeeServiceImpl = null;
	private List<Employee> employees = new ArrayList<Employee>();

	private EmployeeServiceImpl() {
		this.employees.addAll(this.getSeedData());
	}

	public static EmployeeServiceImpl getInstance() {
		if (employeeServiceImpl == null) {
			synchronized (EmployeeServiceImpl.class) {
				if (employeeServiceImpl == null) {
					employeeServiceImpl = new EmployeeServiceImpl();
				}
			}
		}
		return employeeServiceImpl;
	}

	public List<Employee> getEmployees() {
		return this.employees;
	}

	public Employee addEmployee(Employee employee) {
		if (employee != null) {
			this.employees.add(employee);
		}
		return employee;
	}

	public boolean checkEmail(String email) throws DuplicationEmailException {
		boolean isfound = this.employees.stream().filter(e -> e.getEmail().equals(email)).findAny().isPresent();
		if (isfound) {
			throw new DuplicationEmailException();
		}
		return isfound;
	}

	@Override
	public List<Employee> sort(Comparator<Employee> comparator) {
		Collections.sort(employees, comparator);
		return employees;
	}

	@Override
	public Employee getEmployee(int empno) {
		Employee emp = employees.stream().filter(e -> e.getEmpno() == empno).findAny().orElse(null);
		return emp;
	}

	@Override
	public boolean updateEmployee(int empno, Employee employee) {
		ListIterator<Employee> iterator = employees.listIterator();
		while (iterator.hasNext()) {
			Employee emp = iterator.next();
			if (emp.getEmpno() == empno) {
				iterator.set(employee);
				return true;
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(int empno) {
		ListIterator<Employee> iterator = employees.listIterator();
		while (iterator.hasNext()) {
			Employee emp = iterator.next();
			if (emp.getEmpno() == empno) {
				iterator.remove();
				break;
			}
		}
	}

	@Override
	public List<Employee> searchData(String str) {

		List<Employee> searchList = employees.stream()
				.filter(e -> e.getEname().contains(str) || e.getEmail().contains(str) || e.getMobile().contains(str))
				.collect(Collectors.toList());
		return searchList;
	}

	private Collection<? extends Employee> getSeedData() {
		Employee e1 = new Employee.EmployeeBuilder().withEname("Rajesh").withEmail("rajesh.t@lms.com")
				.withMobile("9998887771").withQualification("M.Tech").withSalary(49000.0).build();
		Employee e2 = new Employee.EmployeeBuilder().withEname("Suresh").withEmail("suresh.s@lms.com")
				.withMobile("9998887772").withQualification("Ph.D").withSalary(89000.0).build();
		Employee e3 = new Employee.EmployeeBuilder().withEname("Ramesh").withEmail("ramesh.r@lms.com")
				.withMobile("9998887773").withQualification("M.Tech").withSalary(49000.0).build();
		Employee e4 = new Employee.EmployeeBuilder().withEname("Jayesh").withEmail("jayesh.s@lms.com")
				.withMobile("9998887774").withQualification("B.Tech").withSalary(39000.0).build();
		Employee e5 = new Employee.EmployeeBuilder().withEname("Kamesh").withEmail("kamesh.k@lms.com")
				.withMobile("9998887775").withQualification("M.Tech").withSalary(49000.0).build();
		Employee e6 = new Employee.EmployeeBuilder().withEname("Ramit").withEmail("ramit.r@lms.com")
				.withMobile("9998887776").withQualification("M.Tech").withSalary(49000.0).build();
		Employee e7 = new Employee.EmployeeBuilder().withEname("Sumith").withEmail("sumit.s@lms.com")
				.withMobile("9998887777").withQualification("Ph.D").withSalary(99000.0).build();
		Employee e8 = new Employee.EmployeeBuilder().withEname("Kirmit").withEmail("kirmit.k@lms.com")
				.withMobile("9998887778").withQualification("M.Tech").withSalary(49000.0).build();
		Employee e9 = new Employee.EmployeeBuilder().withEname("Balu").withEmail("balu.b@lms.com")
				.withMobile("9998887779").withQualification("M.Tech").withSalary(49000.0).build();
		Employee e10 = new Employee.EmployeeBuilder().withEname("Sony").withEmail("sony.s@lms.com")
				.withMobile("9998887780").withQualification("M.Tech").withSalary(49000.0).build();
		Employee e11 = new Employee.EmployeeBuilder().withEname("Deepu").withEmail("deepu.d@lms.com")
				.withMobile("9998887781").withQualification("B.Tech").withSalary(38000.0).build();
		return Arrays.asList(new Employee[] { e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11 });
	}

}