package com.lms.ems.service;

import com.lms.ems.domain.Employee;
import com.lms.ems.exception.DuplicationEmailException;

import java.util.Comparator;
import java.util.List;

public interface EmployeeService {
	public Employee addEmployee(Employee paramEmployee);

	public List<Employee> getEmployees();

	public boolean checkEmail(String paramString) throws DuplicationEmailException;

	public List<Employee> sort(Comparator<Employee> comparator);

	public Employee getEmployee(int empno);

	public boolean updateEmployee(int empno, Employee employee);

	public void deleteEmployee(int empno);

	public List<Employee> searchData(String str);
}