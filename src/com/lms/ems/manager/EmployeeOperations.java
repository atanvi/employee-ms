package com.lms.ems.manager;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.lms.ems.domain.Employee;
import com.lms.ems.exception.DuplicationEmailException;
import com.lms.ems.service.EmployeeService;
import com.lms.ems.service.EmployeeServiceImpl;

public class EmployeeOperations {
	EmployeeService employeeService = EmployeeServiceImpl.getInstance();
	Scanner sc = null;

	public void start() {
		this.sc = new Scanner(System.in);
		do {
			System.out.println(
					"\n---------------------------Employee Management System-------------------------------------\n");
			System.out.println("1.ADD\t2.VIEW ALL\t3.EDIT\t4.DELETE\t5.SEARCH\t6.SORT\t7.EXIT ");
			System.out.println(
					"\n------------------------------------------------------------------------------------------\n");
			int choice = this.getUserChoice();
			switch (choice) {
			case 1:
				this.addEmployee();
				break;

			case 2:
				this.viewEmployees();
				break;
			case 3:
				this.editEmployee();
				break;
			case 4:
				this.deleteEmployee();
				break;
			case 5:
				this.searchEmployee();
				break;
			case 6:
				this.sortEmployee();
				break;

			case 7: {
				System.out.println("Bye............");
				this.sc.close();
				System.exit(0);
			}
			}
		} while (true);
	}

	private void sortEmployee() {

		System.out.println("Sort : 1. Name 2. Email 3. Salary ");
		boolean isNumber = true;
		int choice = 0;
		try {
			choice = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			isNumber = false;
		}
		while (!(choice > 0 && choice < 4 || isNumber)) {
			System.out.println("Choice must be 1 to 3 only");
			try {
				choice = Integer.parseInt(this.sc.nextLine());
			} catch (NumberFormatException e) {
				isNumber = false;
			}
		}
		List<Employee> list=null;
		if (choice == 1) {
			list = employeeService.sort((e1, e2) -> e1.getEname().compareTo(e2.getEname()));
		} else if (choice == 2) {
			list = employeeService.sort((e1, e2) -> e1.getEmail().compareTo(e2.getEmail()));
		} else if (choice == 3) {
			list = employeeService
					.sort((e1, e2) -> e1.getSalary() > e2.getSalary() ? 1 : e1.getSalary() < e2.getSalary() ? -1 : 0);
		}
		System.out.println("Sorting is done successfully ");
		for(Employee employee:list){
			printEmployee(employee);
		}

	}

	private void searchEmployee() {
		System.out.println("Enter the search string (name, email, mobile )");
		String str = sc.nextLine();
		List<Employee> list=employeeService.searchData(str);
		if(list!=null && !list.isEmpty()){
			System.out.println("Please wait.................");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			System.out.println("Search result :");
			list.stream().forEach(e->printEmployee(e));
		}else{
			System.out.println("There are no matched records found for given search data");
		}
	}

	private void deleteEmployee() {
		int empno=getUserInputEmpno();
		Employee emp=employeeService.getEmployee(empno);
		while(emp==null){
			System.out.println("With the entred empno, there is no employee...");
			empno = getUserInputEmpno();
			emp=employeeService.getEmployee(empno);
		}
		
		System.out.println("Are sure do want to delete employee (y/n) ");
		String choice=sc.nextLine();
		if(choice.equalsIgnoreCase("y")){
			employeeService.deleteEmployee(empno);
			System.out.println("Employee with empno :"+empno+" is deleted successfuly ");
		}

	}

	private void editEmployee() {
		int empno=getUserInputEmpno();
		Employee emp=employeeService.getEmployee(empno);
		while(emp==null){
			empno = getUserInputEmpno();
			emp=employeeService.getEmployee(empno);
		}
		System.out.println("Employee details to edit:");
		printEmployee(emp);
		System.out.println("\nUpdate 1.Mobile 2. Qualification 3. Salary 4. Nothing");
		boolean isNumber = true;
		int choice = 0;
		try {
			choice = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			isNumber = false;
		}
		while (!(choice > 0 && choice < 5 || isNumber)) {
			System.out.println("Choice must be 1 to 4 only");
			try {
				choice = Integer.parseInt(this.sc.nextLine());
			} catch (NumberFormatException e) {
				isNumber = false;
			}
		}
		
	    String mobile=null,qualification=null;
	    double salary=0.0;
		if(choice==1){
			 mobile = getValidMobileInput();
		}else if(choice == 2){
			 qualification = getQulificationInput();
		}else if(choice == 3){
			 salary = getSalaryInput();
		}else if(choice==4){
			return;
		}
			
		Employee employee = new Employee.EmployeeBuilder().withEname(emp.getEname()).withMobile(mobile==null?emp.getMobile():mobile).withEmail(emp.getEmail())
				.withQualification(qualification==null?emp.getQulification():qualification).withSalary(salary==0.0?emp.getSalary():salary).build();
		
		employeeService.updateEmployee(emp.getEmpno(),employee);
		
	}

	private int getUserInputEmpno() {
		boolean isNumber = false;
		int empno = 0;
		System.out.println("Enter the Empno :");
		try {
			empno = Integer.parseInt(this.sc.nextLine());
		} catch (NumberFormatException e) {
			isNumber = true;
		}
		while (empno <= 0 && isNumber) {
			System.out.println("Enter valid Empno : must be > 0");
			try {
				empno = Integer.parseInt(this.sc.nextLine());
			} catch (NumberFormatException e) {
				isNumber = true;
			}
		}
		return empno;
	}

	private void viewEmployees() {
		List<Employee> list = this.employeeService.getEmployees();
		list.stream().forEach(e -> printEmployee(e));

	}

	private void printEmployee(Employee employee) {
		System.out.format("\n%-8d%-10s%-12s%-20s%-10s% 6.2f\n", employee.getEmpno(), employee.getEname(),
				employee.getMobile(), employee.getEmail(), employee.getQulification(), employee.getSalary());
	}

	private void addEmployee() {
		System.out.println("Enter the name :");
		String ename = this.sc.nextLine();
		String email = this.checkUserEmail();
		String mobile = this.getValidMobileInput();
		String qualification = this.getQulificationInput();
		double salary = this.getSalaryInput();
		Employee employee = new Employee.EmployeeBuilder().withEname(ename).withMobile(mobile).withEmail(email)
				.withQualification(qualification).withSalary(salary).build();
		System.out.println("Please wait .......................... ");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		employee = this.employeeService.addEmployee(employee);
		if (employee != null) {
			System.out.println("Employee is added successfully with id :" + employee.getEmpno());
		} else {
			System.out.println("Something went wrong while adding employee");
		}
	}

	private double getSalaryInput() {
		boolean isNumber = false;
		double salary = 0.0;
		System.out.println("Enter the salary :");
		try {
			salary = Double.parseDouble(this.sc.nextLine());
		} catch (NumberFormatException e) {
			isNumber = true;
		}
		while (salary <= 0.0 && isNumber) {
			System.out.println("Enter valid salary : Must be > 0");
			try {
				salary = Double.parseDouble(this.sc.nextLine());
				System.out.println("Salary :"+salary);
			} catch (NumberFormatException e) {
				isNumber = true;
			}
			
		}
		return salary;
	}

	private String getQulificationInput() {
		System.out.println("Enter the Qualification :");
		String qulification = this.sc.nextLine();
		return qulification;
	}

	private String getValidMobileInput() {
		System.out.println("Enter the mobile number :");
		String mobile = this.sc.nextLine();
		while (!this.checkContainDigits(mobile)) {
			System.out.println("Enter the valid mobile number :");
			mobile = this.sc.nextLine();
		}
		return mobile;
	}

	private boolean checkContainDigits(String mobile) {
		Pattern pattern = Pattern.compile("\\d{10}");
		Matcher matcher = pattern.matcher(mobile);
		boolean isvalid = matcher.matches();
		return isvalid;
	}

	private String checkUserEmail() {
		boolean isEmailExits = false;
		boolean isValid = false;
		System.out.println("Enter the email : Ex (abc@gmail.com) :");
		String email = this.sc.nextLine();
		while (!isValid) {
			isValid = this.isValidEmail(email);
			if (isValid)
				break;
			System.out.println("Enter the email : Ex (abc@gmail.com) :");
			email = this.sc.nextLine();
		}
		try {
			this.employeeService.checkEmail(email);
		} catch (DuplicationEmailException e) {
			System.out.println("Email :" + email + " already assign to some other employee \n");
			isEmailExits = true;
		}
		if (isEmailExits) {
			this.checkUserEmail();
		}
		return email;
	}

	private boolean isValidEmail(String email) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	private int getUserChoice() {
		boolean isNumber = true;
		int choice = 0;
		try {
			choice = Integer.parseInt(this.sc.nextLine());
		} catch (NumberFormatException e) {
			isNumber = false;
		}
		while (!(choice > 0 && choice < 8 || isNumber)) {
			System.out.println("Choice must be 1 to 7 only");
			try {
				choice = Integer.parseInt(this.sc.nextLine());
			} catch (NumberFormatException e) {
				isNumber = false;
			}
		}
		return choice;
	}
}
