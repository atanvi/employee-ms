package com.lms.ems.manager;

public class Main
{
  public static void main(String[] args)
  {
    EmployeeOperations employeeOperations = new EmployeeOperations();
    employeeOperations.start();
  }
}