package com.lms.ems.domain;

public abstract class IDGenerator
{
  private static int empno = 1001;
  
  public static int getNewId()
  {
    return empno++;
  }
}
