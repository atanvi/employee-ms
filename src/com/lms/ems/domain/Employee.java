package com.lms.ems.domain;

public class Employee
{
  private int empno;
  private String ename;
  private String email;
  private String mobile;
  private String qulification;
  private double salary;
  
  private Employee(EmployeeBuilder builder)
  {
    this.empno = IDGenerator.getNewId();
    this.ename = builder.ename;
    this.email = builder.email;
    this.mobile = builder.mobile;
    this.qulification = builder.qulification;
    this.salary = builder.salary;
  }
  
  public int getEmpno()
  {
    return this.empno;
  }
  
  public void setEmpno(int empno)
  {
    this.empno = empno;
  }
  
  public String getEname()
  {
    return this.ename;
  }
  
  public void setEname(String ename)
  {
    this.ename = ename;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public void setEmail(String email)
  {
    this.email = email;
  }
  
  public String getMobile()
  {
    return this.mobile;
  }
  
  public void setMobile(String mobile)
  {
    this.mobile = mobile;
  }
  
  public String getQulification()
  {
    return this.qulification;
  }
  
  public void setQulification(String qulification)
  {
    this.qulification = qulification;
  }
  
  public double getSalary()
  {
    return this.salary;
  }
  
  public void setSalary(double salary)
  {
    this.salary = salary;
  }
  
  public static class EmployeeBuilder
  {
    private String ename;
    private String email;
    private String mobile;
    private String qulification;
    private double salary;
    
    public EmployeeBuilder withEname(String ename)
    {
      this.ename = ename;
      return this;
    }
    
    public EmployeeBuilder withEmail(String email)
    {
      this.email = email;
      return this;
    }
    
    public EmployeeBuilder withMobile(String mobile)
    {
      this.mobile = mobile;
      return this;
    }
    
    public EmployeeBuilder withQualification(String qualification)
    {
      this.qulification = qualification;
      return this;
    }
    
    public EmployeeBuilder withSalary(double salary)
    {
      this.salary = salary;
      return this;
    }
    
    public Employee build()
    {
      return new Employee(this);
    }
  }
}
