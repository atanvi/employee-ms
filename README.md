Employee Management System:
*********************************************************************************************************************

This project is developed without using GUI, which provides CLI to perform CRUD operations on Employee domain object.


Concepts used:
********************************************************************************************************************
Object Oriented Programming 
Exception handling
Interfaces
Collection Framework
Regular Expression


Java Version:
*********************************************************************************************************************
Java -8 

---------------------------------------------------------------------------------------------------------------------
Note:   *
*********

This project does not contain any database, completely implemented by using container class (In-memory).

Download Project:
****************

Import project as general from bitbucket reference link and convert to JavaProject from your IDE (eclipse, netbeans)



